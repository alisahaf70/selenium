# -*- coding: utf-8 -*-
from selenium import webdriver
from random import randint
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from sys import platform
from selenium.webdriver.firefox.firefox_binary import FirefoxBinary
import time
RandomOnly=False
class AutoBuy:
    def __init__(self):
        if platform == "win32":
            fp = webdriver.FirefoxProfile("C:/Users/Alireza/AppData\Roaming/Mozilla/Firefox/Profiles/t049907o.default-1482180878097")
            binary = FirefoxBinary(r'C:\Program Files (x86)\Mozilla Firefox\firefox.exe')
            self.driver = webdriver.Firefox(firefox_binary=binary, firefox_profile=fp)
        else:
            fp = webdriver.FirefoxProfile('/home/alireza/.mozilla/firefox/cw09azbd.default')
            self.driver = webdriver.Firefox(fp)
        self.driver.get("http://www.digistyle.com/Account/Login")
        inputElement = self.driver.find_element_by_id("Email")
        inputElement.send_keys('m.sahaf@live.com')
        inputElement = self.driver.find_element_by_id("Password")
        inputElement.send_keys('33920148')
        self.driver.find_element_by_class_name("dk-btn-pink").click()
    def StartEngine(self,url):
        self.driver.get(url)
        a=self.driver.find_elements_by_name("product_size")
        lst=[]
        Clicked=False
        for it in a:
            if it.is_displayed():
                lst.append(it)
                if ~RandomOnly and (it.get_attribute("id")=="size_103" or it.get_attribute("id")=="size_6" or it.get_attribute("id")=="size_96" or it.get_attribute("id")=="size_17" or it.get_attribute("id")=="size_60"):
                    it.click()
                    Clicked=True
        if len(lst)!=0 and Clicked==False:
            lst[randint(0,len(lst))-1].click()
        self.driver.find_element_by_class_name("add-to-cart").click()
        try:
            element = WebDriverWait(self.driver, 5).until(EC.presence_of_element_located((By.CLASS_NAME, "next-step")))
        finally:
            self.driver.find_element_by_class_name("next-step").click()
            try:
                element = WebDriverWait(self.driver, 5).until(EC.presence_of_element_located((By.NAME, "shipping_time")))
            finally:
                elements=self.driver.find_elements_by_name("shipping_time")
                for item in elements:
                    if(item.get_attribute("disabled")=="true"):
                        continue
                    else:
                        item.click()
                        break
                self.driver.find_element_by_class_name("next-step").click()
                try:
                    element = WebDriverWait(self.driver, 5).until(EC.presence_of_element_located((By.NAME, "payment_gateway")))
                finally:
                    self.driver.find_element_by_name("payment_gateway").click()
                    self.driver.find_element_by_class_name("next-step").click()
        self.driver.close()