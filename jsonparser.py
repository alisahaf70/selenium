import webbrowser
import requests
import DigiStyleAuto
import time

Run=True
price=10000
buyer=DigiStyleAuto.AutoBuy()
pagesize=6000

while Run:
    try:
        print ("Download Started")
        processedItems=0
        for i in range(1,int(6000/pagesize)+1):
            Downloaded = False
            while Downloaded==False:
                try:
                    r=requests.get('http://search.digistyle.com/api2/Data/GetCollection?collectionId=1&pageIndex='+str(i)+'&pageSize='+str(pagesize+1), timeout=10)
                    Downloaded=True
                except requests.exceptions.Timeout:
                    print("Retry Timeout")
                    Downloaded=False
                except requests.exceptions.TooManyRedirects:
                    print("Retry TooManyRedirects")
                    Downloaded = False
                except requests.exceptions.RequestException as e:
                    print("Retry exceptions")
                    Downloaded = False
            print ("Parsing JSON")
            try:
                data = r.json()
            except:
                i=max(1,i-1)
                continue;
            if data["hits"]["total"] == 0:
                break
            print ("Searching For Discounted Item in page ("+str(i)+")")
            processedItems=processedItems+len(data["hits"]["hits"])
            for k in data["hits"]["hits"]:
                c=k["_source"]["MinPrice"]
                D=k["_source"]["MinPriceList"]
                if c>0 and D>0 and (c<=price or D<=price) and k["_source"]["Id"]!=20182:
                    url="http://www.digistyle.com/Product/"+k["_source"]["FaTitle"].replace(" ","-")+"-"+str(k["_source"]["Id"])
                    buyer.StartEngine(url)
                    webbrowser.open_new(url)
                    Run=False
                    break;
        print ("Iteration Finished(List Size="+str(processedItems)+")")
    except:
        time.sleep(1)